package main

import (
	"flag"
	"log"
	"net"
	"net/http"

	"gitlab.com/aevstifeev/modsecurityd/server"
)

var (
	rulesPath = flag.String("rules", "", "path for the rules file")
	unix      = flag.Bool("unix", false, "listen on unix socket")
	addr      = flag.String("addr", ":8080", "addr to listen on")
)

func main() {
	flag.Parse()

	srv, err := server.New(*rulesPath)
	if err != nil {
		log.Fatal("Failed to init server: ", err)
	}
	defer srv.Close()

	log.Println("Listening on", *addr)
	if !*unix {
		if err := http.ListenAndServe(*addr, srv); err != nil {
			log.Fatal("Failed to start server: ", err)
		}
		return
	}

	l, err := net.Listen("unix", *addr)
	if err != nil {
		log.Fatal("Failed to open socket listener: ", err)
	}

	if err := http.Serve(l, srv); err != nil {
		log.Fatal("Failed to start server: ", err)
	}
}
