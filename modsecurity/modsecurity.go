package modsecurity

import (
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"sync"
	"unsafe"
)

/*
#cgo CFLAGS: -I/usr/local/modsecurity/include
#cgo LDFLAGS: -Wl,-rpath,/usr/local/modsecurity/lib -L/usr/local/modsecurity/lib -lmodsecurity
#include "modsecurity/modsecurity.h"
#include "modsecurity/transaction.h"
#include "modsecurity/rules.h"

extern void modsec_log_cb(void *trID, char* data);

static inline void modsecurity_log(void *log, const void* data) {
  modsec_log_cb(log, (char *)data);
}

static inline ModSecurity *modsec_init_with_log() {
  ModSecurity *modsec = msc_init();
  msc_set_log_cb(modsec, modsecurity_log);

  return modsec;
}
*/
import "C"

var (
	cbMutex sync.Mutex
	cbStore = map[unsafe.Pointer]*Transaction{}
)

func registerTransaction(tr *Transaction) unsafe.Pointer {
	var ptr unsafe.Pointer = C.malloc(C.size_t(1))
	if ptr == nil {
		panic("failed to allocate cgo pointer")
	}

	cbMutex.Lock()
	cbStore[ptr] = tr
	cbMutex.Unlock()

	return ptr
}

//export modsec_log_cb
func modsec_log_cb(trID unsafe.Pointer, data *C.char) {
	if trID == nil {
		return
	}

	cbMutex.Lock()
	tr := cbStore[trID]
	if tr != nil {
		tr.result.Logs = append(tr.result.Logs, C.GoString(data))
	}
	cbMutex.Unlock()
}

type ModSecurity struct {
	modsec *C.struct_ModSecurity_t
}

func New() *ModSecurity {
	return &ModSecurity{C.modsec_init_with_log()}
}

func (ms *ModSecurity) Cleanup() {
	C.msc_cleanup(ms.modsec)
}

func (ms *ModSecurity) NewTransaction(rs *RulesSet) *Transaction {
	tr := &Transaction{result: &TransactionResult{Logs: make([]string, 0)}}
	tr.id = registerTransaction(tr)
	tr.transaction = C.msc_new_transaction(ms.modsec, rs.rules, tr.id)
	return tr
}

func (ms *ModSecurity) ProcessRequest(rs *RulesSet, req *http.Request) (*TransactionResult, error) {
	tr := ms.NewTransaction(rs)
	defer tr.Cleanup()

	if err := tr.ProcessConnection(req.RemoteAddr, req.URL); err != nil {
		return nil, err
	}

	if err := tr.ProcessRequestHeaders(req.Header); err != nil {
		return nil, err
	}

	if err := tr.ProcessRequestBody(req.Body); err != nil {
		return nil, err
	}

	return tr.Result(), nil
}

/*
  Transaction
*/
type Transaction struct {
	id          unsafe.Pointer
	transaction *C.struct_Transaction_t
	result      *TransactionResult
}

type TransactionResult struct {
	Logs         []string
	Intervention *Intervention
}

type Intervention struct {
	Status     int
	URL        string
	Log        string
	Disruptive bool
}

func (tr *Transaction) Cleanup() {
	cbMutex.Lock()
	delete(cbStore, tr.id)
	cbMutex.Unlock()

	C.free(tr.id)
	C.msc_transaction_cleanup(tr.transaction)
}

func (tr *Transaction) ProcessConnection(remoteAddr string, sURL *url.URL) error {
	sHost, port, err := net.SplitHostPort(sURL.Host)
	sPort, err := strconv.Atoi(port)
	if err != nil {
		if sURL.Scheme == "https" {
			sPort = 443
		} else {
			sPort = 80
		}
	}

	cHost, port, _ := net.SplitHostPort(remoteAddr)
	cPort, _ := strconv.Atoi(port)

	cHostCS := C.CString(cHost)
	defer C.free(unsafe.Pointer(cHostCS))

	sHostCS := C.CString(sHost)
	defer C.free(unsafe.Pointer(sHostCS))

	if ret := C.msc_process_connection(tr.transaction, cHostCS, C.int(cPort), sHostCS, C.int(sPort)); ret != 1 {
		return fmt.Errorf("failed to extract connection information")
	}

	return nil
}

func (tr *Transaction) ProcessRequestHeaders(headers http.Header) error {
	for key := range headers {
		value := headers.Get(key)
		keyCS := (*C.uchar)(unsafe.Pointer(C.CString(key)))
		valueCS := (*C.uchar)(unsafe.Pointer(C.CString(value)))
		C.msc_add_request_header(tr.transaction, keyCS, valueCS)
		C.free(unsafe.Pointer(keyCS))
		C.free(unsafe.Pointer(valueCS))
	}

	if ret := C.msc_process_request_headers(tr.transaction); ret != 1 {
		return fmt.Errorf("failed to process request headers")
	}

	return nil
}

func (tr *Transaction) ProcessRequestBody(body io.Reader) error {
	tmpfile, err := ioutil.TempFile("", "request_body")
	if err != nil {
		return fmt.Errorf("failed to create temp file: %s", err)
	}
	defer os.Remove(tmpfile.Name())

	if _, err := io.Copy(tmpfile, body); err != nil {
		return fmt.Errorf("failed to write body to the temp file: %s", err)
	}

	pathCS := C.CString(tmpfile.Name())
	defer C.free(unsafe.Pointer(pathCS))
	C.msc_request_body_from_file(tr.transaction, pathCS)

	if ret := C.msc_process_request_body(tr.transaction); ret != 1 {
		return fmt.Errorf("failed to process request headers")
	}

	return nil
}

func (tr *Transaction) Result() *TransactionResult {
	intervention := C.struct_ModSecurityIntervention_t{}
	intervention.status = 200
	intervention.url = nil
	intervention.log = nil
	intervention.disruptive = 0

	if C.msc_intervention(tr.transaction, &intervention) == 0 {
		return tr.result
	}

	tr.result.Intervention = &Intervention{
		int(intervention.status),
		C.GoString(intervention.url),
		C.GoString(intervention.log),
		intervention.disruptive > 0,
	}

	return tr.result
}

/*
  Rules
*/
type RulesSet struct {
	rules *C.struct_Rules_t
}

func NewRulesSet() *RulesSet {
	rules := C.msc_create_rules_set()
	return &RulesSet{rules}
}

func (rs *RulesSet) Cleanup() {
	C.msc_rules_cleanup(rs.rules)
}

func (rs *RulesSet) AddFile(rulesPath string) error {
	path := C.CString(rulesPath)
	defer C.free(unsafe.Pointer(path))

	err := C.CString("")
	defer C.free(unsafe.Pointer(err))

	ret := C.msc_rules_add_file(rs.rules, path, &err)
	if ret < 0 {
		return fmt.Errorf("failed to add rules from the file: %s", C.GoString(err))
	}

	return nil
}
