package server

import (
	"log"
	"net/http"
	"strings"

	"gitlab.com/aevstifeev/modsecurityd/modsecurity"
)

type Server struct {
	modsec *modsecurity.ModSecurity
	rs     *modsecurity.RulesSet
}

func New(rulesPath string) (*Server, error) {
	modsec := modsecurity.New()
	rs := modsecurity.NewRulesSet()

	if err := rs.AddFile(rulesPath); err != nil {
		return nil, err
	}

	return &Server{modsec, rs}, nil
}

func (s *Server) Close() error {
	s.modsec.Cleanup()
	s.rs.Cleanup()
	return nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	result, err := s.modsec.ProcessRequest(s.rs, req)
	if err != nil {
		log.Println("Failed to process request:", err)
		w.WriteHeader(http.StatusOK)
		return
	}

	log.Println(strings.Join(result.Logs, "\n"))

	intervention := result.Intervention
	if intervention == nil {
		w.WriteHeader(http.StatusOK)
		return
	}

	log.Printf("Responding with %d due to %s", intervention.Status, intervention.Log)
	w.WriteHeader(intervention.Status)
}
